﻿/*_______ ______ __  __ _____  _            _______ ______   ____   ____   ____ _______ 
 |__   __|  ____|  \/  |  __ \| |        /\|__   __|  ____| |  _ \ / __ \ / __ \__   __|
 | |  | |__  | \  / | |__) | |       /  \  | |  | |__    | |_) | |  | | |  | | | |   
 | |  |  __| | |\/| |  ___/| |      / /\ \ | |  |  __|   |  _ <| |  | | |  | | | |   
 | |  | |____| |  | | |    | |____ / ____ \| |  | |____  | |_) | |__| | |__| | | |   
 |_|  |______|_|  |_|_|    |______/_/    \_\_|  |______| |____/ \____/ \____/  |_|   
 */


        /* Função de aparecer ou desaparecer o menu lateral */
        $("#menu-toggle").click(function (e) {
    $("#wrapper").toggleClass("toggled");
    e.preventDefault();
});

$('#side-menu').metisMenu();

/*   _______ ______ __  __ _____  _            _______ ______   ____   ____   ____ _______ 
 // |__   __|  ____|  \/  |  __ \| |        /\|__   __|  ____| |  _ \ / __ \ / __ \__   __|
 //    | |  | |__  | \  / | |__) | |       /  \  | |  | |__    | |_) | |  | | |  | | | |   
 //    | |  |  __| | |\/| |  ___/| |      / /\ \ | |  |  __|   |  _ <| |  | | |  | | | |   
 //    | |  | |____| |  | | |    | |____ / ____ \| |  | |____  | |_) | |__| | |__| | | |   
 //    |_|  |______|_|  |_|_|    |______/_/    \_\_|  |______| |____/ \____/ \____/  |_|    
 */

/*__  __  ____  _    _  _____ ______   _______ _____            _____  
 |  \/  |/ __ \| |  | |/ ____|  ____| |__   __|  __ \     /\   |  __ \ 
 | \  / | |  | | |  | | (___ | |__       | |  | |__) |   /  \  | |__) |
 | |\/| | |  | | |  | |\___ \|  __|      | |  |  _  /   / /\ \ |  ___/ 
 | |  | | |__| | |__| |____) | |____     | |  | | \ \  / ____ \| |     
 |_|  |_|\____/ \____/|_____/|______|    |_|  |_|  \_\/_/    \_\_|   
 */

/* Atalho, Ao apertar alt, foca no primeiro ítem do menu lateral */
Mousetrap.bind('alt', function (e) {
    e.preventDefault();
    $('#side-menu li:first-child a').focus();
    $("#wrapper").removeClass("toggled");
    e.preventDefault();
});

Mousetrap.bind("alt+1", function (e) {
    $("#wrapper").addClass("toggled");
    e.preventDefault();
});

Mousetrap.bind("alt+2", function (e) {
    $('#menu-usuario > li').addClass('open');
    $('#menu-usuario > li >a').focus();
    e.preventDefault();
});

/*  _   __  __  ____  _    _  _____ ______   _______ _____            _____  
 |  \/  |/ __ \| |  | |/ ____|  ____| |__   __|  __ \     /\   |  __ \ 
 | \  / | |  | | |  | | (___ | |__       | |  | |__) |   /  \  | |__) |
 | |\/| | |  | | |  | |\___ \|  __|      | |  |  _  /   / /\ \ |  ___/ 
 | |  | | |__| | |__| |____) | |____     | |  | | \ \  / ____ \| |     
 |_|  |_|\____/ \____/|_____/|______|    |_|  |_|  \_\/_/    \_\_|     
 */


$('a').on('click', function (e) {
    return true;
    e.preventDefault();
    var target = $(this).attr('href');
    var method = $(this).data('method');

    if (typeof target === 'undefined' || target === '') {
        return true;
    }

    if (method === 'reload') {
        return true;
    }

    if (typeof method === 'undefined') {
        method = 'GET';
    }

    $.ajax({
        method: method,
        url: target
    }).done(function (msg) {
        $('#page-minor-wrapper').html(msg);
        history.replaceState({}, '', target);
    }).fail(function () {
        return true;
        alert('fail');
    }).always(function () {

    })


})