<?php

include_once 'config.php'; // Load configuration info
include_once 'library/common.php'; // Load custom php functions
include_once 'vendor/autoload.php'; //Load Composer libraries

$BREADCRUMB = new Creitive\Breadcrumbs\Breadcrumbs;

if (DATABASE) { //Load database info if defined in config.php
    include_once 'library/database.php';
}

if(AUTH_REQUIRED){
    include_once MODULES_FOLDER . 'core/auth/classes/User.class.php';
    session_start();
    if(isset($_SESSION['user_data']['user'])){
        $USER = $_SESSION['user_data']['user'];
    }
}

/* _____             _  
  |  __ \           | |
  | |__) |___  _   _| |_ ___  ___
  |  _  // _ \| | | | __/ _ \/ __|
  | | \ \ (_) | |_| | ||  __/\__ \
  |_|  \_\___/ \__,_|\__\___||___/
 */

$ROUTES = new \Klein\Klein(); //Defining Routes Object
include 'library/basic_routes.php'; // Load default routes
include_once 'routes.php'; // Load general Routes
include_once 'library/Routes_crawler.class.php'; // Load modular routes finder
Routes_crawler::crawl(); // Load Routes from modules
$ROUTES->dispatch(); // Dispatch Rotes
