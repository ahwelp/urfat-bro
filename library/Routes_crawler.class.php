<?php

class Routes_crawler {

    public static function crawl() {
        if (ROUTING_DEPTH == "MODULAR") {
            Routes_crawler::modular_crawl();
        } else if (ROUTING_DEPTH == "MODULAR_SUB_SYSTEM") {
            Routes_crawler::modular_subsistem_crawl();
        }
    }

    public static function modular_crawl() {
        global $ROUTES;

        $modules = scandir(MODULES_FOLDER);
        foreach ($modules as $module) {
            if ($module != "." && $module != ".." && is_file(MODULES_FOLDER . $module . "declarations/Routes.php")) {
                include MODULES_FOLDER . $module . "declarations/Routes.php";
            }
        }
    }

    public static function modular_subsistem_crawl() {
        global $ROUTES;

        $subsystems = scandir(MODULES_FOLDER);
        foreach ($subsystems as $subsystem) {
            if ($subsystem != "." && $subsystem != "..") {
                $modules = scandir(MODULES_FOLDER . $subsystem);
                foreach ($modules as $module) {
                    if ($module != "." && $module != ".." && is_file(MODULES_FOLDER . $subsystem . '/' . $module . "/declarations/Routes.php")) {
                        include MODULES_FOLDER . $subsystem . '/' . $module . "/declarations/Routes.php";
                    }
                }
            }
        }
    }

}
