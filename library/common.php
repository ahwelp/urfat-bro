<?php

function is_ajax_request() {
    //if (isset(getallheaders()['HTTP_X_REQUESTED_WITH'])) {
    if (isset( $_SERVER['HTTP_X_REQUESTED_WITH'] )) {
        return true;
    } else {
        return false;
    }
}

function load_template($values = null) {
    $location = TEMPLATE_FOLDER . 'router_template/lib.php';    
    if (!file_exists($location)) {
        require_once GENERAL_VIEW_FOLDER . 'errors/404.php';
    }
    require_once( $location );
}

function load_module($location = '', $values = null) {

    if (is_array($values) && count($values) > 0) {
        extract($values, EXTR_PREFIX_SAME, 'data');
    }

    if (file_exists(MODULES_FOLDER . $location . 'Controller.php')) {
        $controller;
        require_once( $location . 'Controller.php' );

        if (method_exists($controller, $values[0])) {
            $controller::{$values[0]}(array_slice($values, 1));
        } else {
            require_once GENERAL_VIEW_FOLDER . 'errors/404.php';
        }
    } else if (file_exists($location . 'script.php')) {
        require_once( $location . 'script.php' );
    } else {
        require_once GENERAL_VIEW_FOLDER . 'errors/404.php';
    }
}

function load_view($location = '', $values = null) {
    
    if (is_array($values) && count($values) > 0) {
        extract($values, EXTR_PREFIX_SAME, 'data');
    }

    if (ROUTING_DEPTH == 'SIMPLE') {
        $location = GENERAL_VIEW_FOLDER . $location;
    } else {
        $location = MODULES_FOLDER . $location;
    }
    $location .= '.php';
    if (!file_exists($location)) {
        return require_once GENERAL_VIEW_FOLDER . 'errors/404.php';
    }
    return require_once( $location );
}

function require_login(){
    global $RESPONSE;
    if(!isset($_SESSION['user_data']['id'])){        
        $RESPONSE->unlock();
        
    }
    $RESPONSE->redirect('/auth');
}
