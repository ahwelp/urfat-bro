<?php

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

/*
  $capsule->addConnection([
  "driver" => "sqlite",
  "database" => APPLICATION_FOLDER."database.sqlite"
  ]); */

$capsule->addConnection([
    'driver' => 'pgsql',
    'host' => 'localhost',
    'database' => 'teste',
    'username' => 'teste',
    'password' => 'teste',
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => '',
]);

/*
  $capsule->addConnection([
  'driver'    => 'mysql',
  'host'      => 'localhost',
  'database'  => 'teste',
  'username'  => 'root',
  'password'  => 'root',
  'charset'   => 'utf8',
  'collation' => 'utf8_unicode_ci',
  'prefix'    => '',
  ]);
 */

$capsule->setAsGlobal();
$capsule->bootEloquent();
