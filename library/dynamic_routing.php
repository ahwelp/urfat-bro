<?php

$request = $_REQUEST['uri'];
$segments = explode("/", $request);

switch (ROUTING_DEPTH) {
    case "MODULAR":
        
        $module = MODULES_FOLDER . "{$segments[0]}/";
        load_module($module);

    case "MODULAR_SUB_SYSTEM":
        
        $module = MODULES_FOLDER . "{$segments[0]}/{$segments[1]}/";
        load_module($module, array_slice($segments, 2) );
        
        break;
    default:
        die;
}

