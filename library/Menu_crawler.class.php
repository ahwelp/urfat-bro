<?php

class Menu_crawler {

    public static function crawl() {
        if (ROUTING_DEPTH == "MODULAR") {
            Menu_crawler::modular_crawl();
        } else if (ROUTING_DEPTH == "MODULAR_SUB_SYSTEM") {
            Menu_crawler::modular_subsistem_crawl();
        }
    }

    public static function modular_crawl() {
        $modules = scandir(MODULES_FOLDER);
        foreach ($modules as $module) {
            if ($module != "." && $module != ".." && is_file(MODULES_FOLDER . $module . "declarations/Menu.php")) {
                include MODULES_FOLDER . $module . "declarations/Menu.php";
            }
        }
    }

    public static function modular_subsistem_crawl() {
        $subsystems = scandir(MODULES_FOLDER);
        foreach ($subsystems as $subsystem) {
            if ($subsystem != "." && $subsystem != "..") {
                $modules = scandir(MODULES_FOLDER . $subsystem);
                foreach ($modules as $module) {
                    if ($module != "." && $module != ".." && is_file(MODULES_FOLDER . $subsystem . '/' . $module . "/declarations/Menu.php")) {
                        include MODULES_FOLDER . $subsystem . '/' . $module . "/declarations/Menu.php";
                    }
                }
            }
        }
    }

}
