<?php

$ROUTES->respond('*', function() {
    global $REQUEST, $RESPONSE, $USER;

    if(!AUTH_REQUIRED){
        return;
    }
    
    if (empty($_SESSION)) {
        //$RESPONSE->redirect('/auth');
        header('Location: /auth');
    }
    
    if(isset($_SESSION['user_data']['user'])) {
        if (!empty($_SESSION['user_data']['user'])) {
            if ($_SESSION['user_data']['user']->has_permission('sistema_acessar') && $REQUEST->pathname() == '/auth') {                
                $USER = $_SESSION['user_data']['user'];
                $RESPONSE->redirect('/');    
            }
        }
    }

    if ($REQUEST->pathname() == '/auth') {
        return;
    }

    if ($REQUEST->pathname() == '/auth/status') {
        return;
    }

    if (AUTH_REQUIRED) {
        if (!empty($_SESSION['user_data']['user']) && $_SESSION['user_data']['user']->has_permission('sistema_acessar')) {
            $USER = $_SESSION['user_data']['user'];
            return;
        }

        header('Location: auth');
        //$RESPONSE->redirect('/auth');
    }
});

$ROUTES->onHttpError(function ($code, $router) {
    switch ($code) {
        default:
            echo $code;
            break;
    }
});
