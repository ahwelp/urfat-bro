<?php

$menu = Array(
    'Painel de controle' => Array(
        'session_icon' => 'fa fa-dashboard',
        'session_link' => '/test/wellcome/list',
    ),
    
    'Geração de gráficos' => Array(
        'session_icon' => 'fa fa-bar-chart-o',
        'session_link' => '#',
        'session_itens' => Array(
            'Gráfico de barras' => Array(
                'session_icon' => 'fa fa-list',
                'session_link' => '/test/wellcome/chart/bar_chart/'
            ),
            'Outros gráficos' => Array(
                'session_icon' => 'fa fa-list',
                'session_link' => '#',
                'session_itens' => Array(
                    'Grágico de pizza' => array(
                        'session_icon' => 'fa fa-pie-chart',
                        'session_link' => '/test/wellcome/list/pie_chart/'
                    ),
                    'Grágico de fluxo' => array(
                        'session_icon' => 'fa fa-line-chart',
                        'session_link' => '/test/sem_modulo'
                    )
                )
            )
        )
    )
);
$_SESSION['system']['menu'][] = $menu;

$menu = Array(
    'Tabelas do sistema' => Array(
        'session_icon' => 'fa fa-bar-table',
        'session_link' => '/test/wellcome',
    ),
    'Geração de gráficos' => Array(
        'session_icon' => 'fa fa-bar-chart-o',
        'session_link' => '#',
        'session_itens' => Array(
            "Gráfico de Duas Barras" => Array(
                'session_icon' => 'fa fa-list',
                'session_link' => '/test/wellcome/chart/bar_chart/'
            )
        )
    )
);
$_SESSION['system']['menu'][] = $menu;