<?php

$ROUTES->respond("GET", "/demo", function(){
    echo DB::table('cards')->get();
});

$ROUTES->respond("GET", "/form/[:id]", function($request){    
    include MODULES_FOLDER.'/demo/demo/DemosController.php';
    $demo = new DemosController();
    $demo->show($request->id);
});

