<?php

class Dashboard {

    function index() {

        global $BREADCRUMB, $USER;

        $BREADCRUMB->addCrumb('Dashboard', '/');

        load_template(array(
            "page_title" => "Dashboard do Sistema",
            "subsystem" => "core",
            "module" => "dashboard",
            "name" => "index",
            "data" => Array())
        );
    }

}
