            <!-- Sidebar -->
            <nav id="sidebar-wrapper">
                <div class="logo" style="">
                    <img src='/media/logo.png' alt="logo" />
                </div>
                <div class="search-nav" style="">
                    <input class="form-control" type="text" placeholder="Busca"/>
                </div>
                <hr />
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="/"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="#" class='has-arrow' aria-expanded="true">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="flot.html">Flot Charts</a>
                            </li>
                            <li>
                                <a href="#" class='has-arrow' aria-expanded="true">
                                    <i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="flot.html">Flot Charts</a>
                                    </li>
                                    <li>
                                        <a href="morris.html">Morris.js Charts</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#" class='has-arrow' aria-expanded="true">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="flot.html">Flot Charts</a>
                            </li>
                            <li>
                                <a href="#" class='has-arrow' aria-expanded="true">
                                    <i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="flot.html">Flot Charts</a>
                                    </li>
                                    <li>
                                        <a href="morris.html">Morris.js Charts</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
                
                <ul id="menu--left" class="nav"><li class="dropdown"><a href="#" class="dropdown-toggle" id="menu-item--cards" data-toggle="dropdown">Cards <b class="caret"></b></a><ul id="menu--cards" class="dropdown-menu"><li><a href="/cards" id="menu-item--listar">Listar</a></li><li class="dropdown"><a href="#" class="dropdown-toggle" id="menu-item--relatorios" data-toggle="dropdown">Relatórios <b class="caret"></b></a><ul id="menu--relatorios" class="dropdown-menu"><li><a href="" id="menu-item--cartas-por-partida">Cartas por partida</a></li></ul></li></ul></li><li><a href="" id="menu-item--olar-2">Olar 2</a></li><li><a href="" id="menu-item--olar">Olar</a></li><li><a href="" id="menu-item--olar">Olar</a></li></ul>
            </nav>
            <!-- /#sidebar-wrapper -->