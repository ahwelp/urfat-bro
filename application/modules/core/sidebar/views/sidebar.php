            <!-- Sidebar -->
            <nav id="sidebar-wrapper">
                <div class="logo" style="">
                    <img src='/media/logo.png' alt="logo" />
                </div>
                <div class="search-nav" style="">
                    <input class="form-control" type="text" placeholder="Busca"/>
                </div>
                <hr />
                
                <?php 
                    global $SIDEBAR;
                    echo $SIDEBAR->render();
                ?>
                
            </nav>
            <!-- /#sidebar-wrapper -->