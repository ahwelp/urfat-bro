<?php

include_once 'classes/User.class.php';

class AuthController {

    function index() {
        var_dump(User::find(3)->all_permissions());
    }

    function teste() {
        global $REQUEST;
        var_dump(User::find($REQUEST->id)->all_permissions());
    }

    function login() {
        $this->execute_login($_POST['username'], $_POST['password']);
    }

    private function execute_login($user = null, $password = null) {
        global $RESPONSE;
        if ($user == null || $password == null) {
            throw new Exception('Missing arguments');
        }
        if (!empty($USER)) {
            throw new Exception('Allready Loggedin');
        }

        $user = User::where('username', $user)->get();

        if (empty($user[0])) {
            throw new Exception("Auth fail login");
        }
        $user = $user[0];
        if ($user->password == md5($password)) {
            $_SESSION['user_data']['user'] = $user;
            $_SESSION['user_data']['permissions'] = $user->all_permissions();
        } else {
            throw new Exception('Auth fail password');
        }
    }

    function logout() {
        session_destroy();
    }

    function has_permission() {
        
    }

    public function status() {
        if (isset($_SESSION['user_data']['user'])) {
            return json_encode(['user' => 'logedin']);
        } else {
            echo json_encode(['user' => 'not_logedin']);
        }
    }

    public function user_info() {
        global $REQUEST, $RESPONSE, $BREADCRUMB;

        $BREADCRUMB->addCrumb('Dashboard', '/')->addCrumb('Usuário', '/user/'.$REQUEST->id);
        
        if (!$_SESSION['user_data']['user']->has_permission('sistema_acessar')) {
            $RESPONSE->redirect('/');
        }

        if ($_SESSION['user_data']['user']->id == $REQUEST->id) {
            //Vissao do proprio usuario
            $template_data = array(
                "page_title" => "Dados de Usuário",
                "subsystem" => "core",
                "module" => "auth",
                "name" => "self_user"
            );
            $template_data['data']['user'] = $_SESSION['user_data']['user'];
            load_template($template_data);
        } else {
            //visao de visitante
        }        
    }

}
