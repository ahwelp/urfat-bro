<?php

$ROUTES->respond("GET", "/auth/permissons", function() {
    include_once __DIR__ . '/../Controller.php';
    $controller = new AuthController();
    $controller->index();
});

$ROUTES->respond("GET", "/auth/status", function() {
    include_once __DIR__ . '/../Controller.php';
    $controller = new AuthController();
    $controller->status();
});

$ROUTES->respond("GET", "/auth/permissons/all/[i:id]", function() {
    include_once __DIR__ . '/../Controller.php';
    $controller = new AuthController();
    $controller->teste();
});

$ROUTES->respond("GET", "/auth/has_permission/[i:id]/[a:permission]", function() {
    global $REQUEST;
    include_once __DIR__ . '/../classes/User.class.php';
    include_once __DIR__ . '/../classes/Role.class.php';
    include_once __DIR__ . '/../classes/Permission.class.php';
    //return User::find($REQUEST->id)->has_permission("qualquer");
    if (User::find($REQUEST->id)->has_permission($REQUEST->permission)) {
        echo 'Tem';
    } else {
        echo 'Não tem';
    };
    //dd(User::find(3)->all_permissions());
});

$ROUTES->respond("POST", "/auth", function() {
    global $RESPONSE;
    include_once __DIR__ . '/../Controller.php';
    $controller = new AuthController();
    try {
        $controller->login();
        //error_log("Redirecionando");
        //$RESPONSE->redirect('/');
        //error_log("Redirecionado");
        header('Location: /');
    } catch (Exception $ex) {
        error_log($ex->getMessage());
    }
});

$ROUTES->respond("GET", "/auth/logout", function() {
    global $RESPONSE;
    session_destroy();
    $RESPONSE->redirect('/auth');
});

$ROUTES->respond("GET", "/auth", function() {
    load_view("core/auth/views/login");
});


$ROUTES->respond("GET", "/user/[i:id]", function(){
    include_once __DIR__ . '/../Controller.php';
    $controller = new AuthController();
    $controller->user_info();
});