<div class="panel panel-default">
    <div class="panel-heading">Dados básicos de usuário</div>
    <div class="panel-body">
        <div class="form-group col-sm-3">
            <label for="">Id de usuário</label>
            <input type="integer" class="form-control" disabled="true" value="<?= $user->id ?>" />
        </div>
        <div class="form-group col-sm-9">
            <label for="">Nome do usuário</label>
            <input type="text" class="form-control" disabled="true" value="<?= $user->name ?>" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">Papeis Atribuidos ao usuário</div>
            <ul class="list-group">
                <?php
                foreach ($user->roles as $role) {
                    echo "<li class='list-group-item'>$role->name</li>";
                }
                ?>
            </ul>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">Permissões Atribuidas ao Usuário</div>
            <ul class="list-group">
                <?php
                foreach ($user->all_permissions() as $permission) {
                    echo "<li class='list-group-item'>$permission->id</li>";
                }
                ?>
            </ul>
        </div>
    </div>
</div>