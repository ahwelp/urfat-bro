<?php

use Illuminate\Database\Eloquent\Model;

include_once 'Role.class.php';
include_once 'Permission.class.php';

class User extends Model {

    public function all_permissions($separadas = false) {
        $all_permissions = Array();

        $roles = $this->roles;
        foreach ($roles as $role) {
            $role_permissions = $role->permissions;
            foreach ($role_permissions as $role_permission) {
                $all_permissions[] = $role_permission->name;
            }
        }
        $single_permissions = $this->permissions;
        foreach ($single_permissions as $single_permission) {
            if ($single_permission->pivot->action == 1) {
                $all_permissions[] = $single_permission->name;
            } else if ($single_permission->pivot->action == 2) {
                $all_permissions = array_diff($all_permissions, Array($single_permission->name));
            }
        }
        return array_unique($all_permissions);
    }

    public function permissions() {
        return $this->belongsToMany(Permission::class)->withPivot('action');
    }

    public function roles() {
        return $this->belongsToMany(Role::class);
    }

    function has_permission($permission = null) {
        $permissions = $this->all_permissions();
        foreach ($permissions as $single_permission) {
            if ($permission == $single_permission || $single_permission == 'administrador') {
                return true;
            }
        }
        return false;
    }

}
