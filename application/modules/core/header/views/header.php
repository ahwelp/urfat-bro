                <header style='min-height: 50px; background-color: #000;'>
                    <a class="btn btn-default" id="menu-toggle" style="margin-top: 5px;">
                        <i class="fa fa-th-list" aria-hidden="true"></i>
                    </a>
                    <ul id='menu-usuario' class="nav navbar-nav navbar-right nav-dropdown">
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-method="reload">
                                <i class="fa fa-user" aria-hidden="true"></i> <span class="caret"></span>
                            </a>
                            <!-- <ul class="dropdown-menu">
                                <li><a href="#">Preferências de usuário</a></li>                                
                                <li><a href="#">Recarregar permissões</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Sair do Sistema</a></li>
                            </ul> -->
                            <?php
                                global $DROPDOWN;
                                echo $DROPDOWN->render();
                            ?>
                        </li>
                    </ul>
                </header>