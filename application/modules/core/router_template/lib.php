<?php

if (APPLICATION_METHOD == 'API' || is_ajax_request()) {
    load_view('core/router_template/views/api', $values);
    die();
}

if (APPLICATION_METHOD == "SYSTEM") {
    global $SIDEBAR, $DROPDOWN;
    
    $SIDEBAR = new Menu(array('id' => 'side-menu', 'class' => "nav"));    
    $DROPDOWN = new Menu(array("class" => 'dropdown-menu'));
    
    include_once LIBRARY_FOLDER.'Menu_crawler.class.php'; // Load modular menu finder
    
    Menu_crawler::crawl();

    load_view('core/router_template/views/system', $values);
}