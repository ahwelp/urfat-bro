<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?= $page_title ?></title>

        <!-- Custom CSS -->
        <link href="/bower_components/normalize-css/normalize.css" rel="stylesheet">
        <link href="/dist/style.css" rel="stylesheet">	
        <link href="/dist/themes/black_and_white.css" rel="stylesheet">

        <link href="/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet">	

        <!--<script src="/bower_components/jquery/dist/jquery.min.js"></script> -->
        
        <script>if (typeof module === 'object') {window.module = module; module = undefined;}</script>
        <script src="/bower_components/jquery/dist/jquery.min.js"></script>
        <script>if (window.module) module = window.module;</script>


        <link href="/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">        
        <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="/bower_components/mousetrap/mousetrap.min.js"></script>

        <link href="/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet"/>


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div id="wrapper">
            <!-- Sidebar -->
            <?php load_module(MODULES_FOLDER . 'core/sidebar/') ?>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="main-page-wrapper">

                <?php load_module(MODULES_FOLDER . 'core/header/') ?>

                <?php load_module(MODULES_FOLDER . 'core/breadcrumb/') ?>

                <div id="page-content-wrapper">

                    <div class="container-fluid" style="height: 1024px;">

                        <div class="row">
                            <div id="page-minor-wrapper" class="col-lg-12">                                
                                <h1><?= $page_title ?></h1>
                                <?php
                                if (isset($module) && isset($name)) {
                                    load_view($subsystem . '/' . $module . '/views/' . $name, $data);
                                } else {
                                    echo $data;
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- /#wrapper -->
        <script src="/bower_components/metisMenu/dist/metisMenu.min.js"></script>
        <script src="/dist/script.js"></script>
    </body>
</html>
