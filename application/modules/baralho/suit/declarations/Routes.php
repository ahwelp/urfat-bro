<?php

$ROUTES->respond("GET", "/suits", function() {
    include_once __DIR__ . '/../Controller.php';
    $controller = new SuitController();
    $controller->index();
});

$ROUTES->respond("GET", "/suits/create", function() {
    include_once __DIR__ . '/../Controller.php';
    $controller = new SuitController();
    $controller->create();
});

$ROUTES->respond("POST", "/suits", function() {
    include_once __DIR__ . '/../Controller.php';
    $controller = new SuitController();
    $controller->store();
});
