<?php

include_once 'classes/Suit.class.php';

class SuitController {

    function index() {
        global $BREADCRUMB, $REQUEST, $RESPONSE, $USER;

        if (!$_SESSION['user_data']['user']->has_permission('sets_visualizar')) {
            //$RESPONSE->redirect('/');            
        }

        $BREADCRUMB->addCrumb('Dashboard', '/')->addCrumb('Naipes', '/suits')->addCrumb('Lista', '/suits');

        $template_data = array(
            "page_title" => "Listagem de Naipes",
            "subsystem" => "baralho",
            "module" => "suit",
            "name" => "list"
        );

        $template_data['data']['suits'] = Suit::all();

        load_template($template_data);
    }

    public function create() {
        global $BREADCRUMB;

        $BREADCRUMB->addCrumb('Dashboard', '/')->addCrumb('Naipes', '/suits')->addCrumb('Novo Naipe', '/suits');

        $template_data = array(
            "page_title" => "Listagem de Naipes",
            "subsystem" => "baralho",
            "module" => "suit",
            "name" => "form",
            'data' => Array('suit' => new Suit())
        );

        load_template($template_data);
    }

    public function store() {
        global $RESPONSE;

        $suit = new Suit();
        $suit->name = $_POST['name'];

        $suit->save();

        $RESPONSE->redirect('/suits');
    }

    public static function as_form() {
        $elements = Suit::all();
        
        $options = Array();
        $options[0] = "Selecione uma opção";
        foreach ($elements as $element) {
            $options[$element->id] = $element->name;            
        }        
        return $options;
    }

}
