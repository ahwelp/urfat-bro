<form action="/suits" method="post" id="form">
    <div class="panel panel-default">
        <div class="panel-heading">Naipe</div>
        <div class="panel-body">
            <div class="form-group col-sm-3">
                <label for="">Id</label>
                <input type="integer" class="form-control" disabled="true" name="id" value="<?= $suit->id ?>" />
            </div>
            <div class="form-group col-sm-9">
                <label for="">Nome do Naipe</label>
                <input type="text" class="form-control" name="name" value="<?= $suit->name ?>" />
            </div>
        </div>
    </div>
    <a onclick="document.getElementById('form').submit();" class="btn btn-success"> Salvar </a>
</form>


