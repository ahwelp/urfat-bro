<a href="/suits/create" class='btn btn-success'>Adicionar novo registro <i class="fa fa-plus" aria-hidden="true"></i></a>
<br /><br />
<div class="panel panel-default">
    <div class="panel-heading">
        Listagem de Naipes
        <a style='float: right' data-toggle="collapse" href="#collapse1"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>        
    </div>
    <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
            <table class='table table-striped table-responsive table-bordered table-hover' style='width: 100%;'>
                <thead style="text-align: center;">
                    <tr>
                        <td>
                            ID
                        </td>
                        <td>
                            Nome
                        </td>
                        <td>
                            Ações
                        </td>
                    </tr>
                </thead>
                <?php
                foreach ($suits as $suit) {
                    echo '<tr>';
                    echo "<td>$suit->id</td>";
                    echo "<td>$suit->name</td>";
                    echo "<td style='text-align:right;'>";
                    echo "  <a href='/cards/$suit->id/edit' title='Editar registro'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>";
                    echo "  <a href='/cards/$suit->id/destroy' title='Apagar Registro'><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>";
                    echo "</td>";
                    echo '</tr>';
                }
                ?>
            </table>
        </div>
        <div class="panel-footer">1/1</div>
    </div>
</div>