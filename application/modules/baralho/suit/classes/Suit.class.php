<?php

use Illuminate\Database\Eloquent\Model;
include_once MODULES_FOLDER . 'baralho/card/classes/Card.php';

class Suit extends Model{
    
    public function cards(){
        return $this->hasMany("Card");
    }
    
}