<?php

global $SIDEBAR, $DROPDOWN;

$menu_cards = MenuItem::newMenu('', 'Cartas', '<i class="fa fa-id-card-o" aria-hidden="true"></i> ');

$menu_cards_actions = new Menu(Array('class'=>'nav nav-second-level'));
$menu_cards_actions->add("/cards", "Listagem de cartas");
$menu_cards_actions->add("/cards/create", "Inserir nova carta");

$menu_cards_actions->add("", "Gráficos");

$menu_cards_actions_list_graph = new Menu(Array('class'=>'nav nav-second-level'));
$menu_cards_actions_list_graph->add('/cards/graph/bars', 'Gráfico de Barras', '<i class="fa fa-bar-chart" aria-hidden="true"></i>');
$menu_cards_actions_list_graph->add('/cards/graph/pie', 'Gráfico de torta', '<i class="fa fa-pie-chart" aria-hidden="true"></i>');

$menu_cards_actions->nestDirect($menu_cards_actions_list_graph);

$menu_cards->nest($menu_cards_actions);

$SIDEBAR->addDirect($menu_cards);