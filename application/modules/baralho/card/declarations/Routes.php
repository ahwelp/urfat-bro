<?php
/* https://github.com/klein/klein.php */
/*
 *   ELOQUENT RESOURCE
 *   GET 	    /photos 	                index 	      photos.index
 *   GET 	    /photos/create 	        create 	      photos.create
 *   POST 	    /photos 	                store 	      photos.store
 *   GET 	    /photos/{photo} 	        show 	      photos.show
 *   GET 	    /photos/{photo}/edit 	edit 	      photos.edit
 *   PUT/PATCH 	    /photos/{photo} 	        update 	      photos.update
 *   DELETE 	    /photos/{photo} 	        destroy       photos.destroy
 * 
 */

$ROUTES->respond("GET", "/cards", function() {     
    include_once __DIR__ . '/../Controller.php';
    $controller = new CardController();
    $controller->index();
});

$ROUTES->respond("GET", "/cards/create", function() {
    include_once __DIR__ . '/../Controller.php';
    $controller = new CardController();
    $controller->create();
});

$ROUTES->respond("POST", "/cards", function() {
    include_once __DIR__ . '/../Controller.php';
    $controller = new CardController();
    $controller->store();
});

$ROUTES->respond("GET", "/cards/[i:id]", function() {
    include_once __DIR__ . '/../Controller.php';
    $controller = new CardController();
    $controller->show();
});

$ROUTES->respond("GET", "/cards/[i:id]/edit", function () {
    include_once __DIR__ . '/../Controller.php';
    $controller = new CardController();
    $controller->edit();
});

$ROUTES->respond("GET", "/cards/[i:id]/destroy", function () {
    include_once __DIR__ . '/../Controller.php';
    $controller = new CardController();
    $controller->destroy();
});

$ROUTES->respond("GET", "/cards/api/list", function() {
    include __DIR__ . '/../classes/Card.php';
    echo Card::all();
});
