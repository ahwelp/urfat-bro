<?php

use Illuminate\Database\Eloquent\Model;
include_once MODULES_FOLDER . 'baralho/suit/classes/Suit.class.php';


class Card extends Model{
    
    public function suit(){
        $this->belongsTo(Suit::class);
    }
}

