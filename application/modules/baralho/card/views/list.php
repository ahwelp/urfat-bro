<a href="/cards/create" class='btn btn-success'>Adicionar novo registro <i class="fa fa-plus" aria-hidden="true"></i></a>
<br /><br />
<div class="panel panel-default">
    <div class="panel-heading">
        Listagem de cartas
        <a style='float: right' data-toggle="collapse" href="#collapse1"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>        
    </div>
    <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
            <table class='table table-striped table-responsive table-bordered table-hover' style='width: 100%;'>
                <thead style="text-align: center;">
                    <tr>
                        <td>
                            ID
                        </td>
                        <td>
                            Nome
                        </td>
                        <td>
                            Naipe
                        </td>                        
                        <td>
                            Ações
                        </td>
                    </tr>
                </thead>
                <?php
                foreach ($cards as $card) {
                    echo '<tr>';
                    echo "<td>$card->id</td>";
                    echo "<td>$card->name</td>";
                    echo "<td>". Suit::where('id', $card->suit)->get()[0]->name."</td>";
                    echo "<td style='text-align:right;'>";
                    echo "  <a href='/cards/$card->id/edit' title='Editar registro'><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>";
                    echo "  <a href='/cards/$card->id/destroy' title='Apagar Registro'><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></a>";
                    echo "</td>";
                    echo '</tr>';
                }
                error_log(print_r($card->suit(), true));
                ?>
            </table>
        </div>
        <div class="panel-footer">1/1</div>
    </div>
</div>