<?php

$builder = new AdamWathan\Form\FormBuilder;
include_once MODULES_FOLDER . 'baralho/suit/Controller.php';

echo $builder->open()->post()->action('/cards');

echo $builder->text('id')->disabled()->value($card->id)->class('col-xs-12');
echo '<br />';
echo $builder->text('nome')->required()->value($card->name);
if($card->suit){    
    echo $builder->select("naipe", SuitController::as_form() )->select($card->suit);
}else{
    echo $builder->select("naipe", SuitController::as_form() );
}

echo '<br />';
echo $builder->submit('Enviar');

?>

