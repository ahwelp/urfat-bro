<?php
include 'classes/Card.php';
class CardController {

    function index() {
        global $BREADCRUMB, $REQUEST, $RESPONSE, $USER;
        
        if(!$_SESSION['user_data']['user']->has_permission('cards_visualizar')){
            $RESPONSE->redirect('/');
            die();
        }

        $BREADCRUMB->addCrumb('Dashboard', '/')->addCrumb('Cartas', '/cards')->addCrumb('Lista', '/cards');

        $template_data = array(
            "page_title" => "Listagem de cartas",
            "subsystem" => "baralho",
            "module" => "card",
            "name" => "list"            
        );

        $template_data['data']['cards'] = Card::all();

        load_template($template_data);
    }

    function edit() {
        global $BREADCRUMB, $REQUEST;
        
        $BREADCRUMB->addCrumb('Dashboard', '/')->addCrumb('Cartas', '/cards')->addCrumb('Editando Registro:', '/cards');

        $carta = Card::where('id', $REQUEST->id)->get()[0];

        $template_data = array(
            "page_title" => "Carta",
            "subsystem" => "baralho",
            "module" => "card",
            "name" => "form",
            "data" => Array(
                "card" => $carta
            )
        );
        load_template($template_data);
    }

    function show() {
        global $REQUEST, $BREADCRUMB;

        $BREADCRUMB->addCrumb('Dashboard', '/')->addCrumb('Cartas', '/cards')->addCrumb('Ítem: ' . $REQUEST->id, '/cards');

        $template_data = array(
            "page_title" => "Carta",
            "subsystem" => "baralho",
            "module" => "card",
            "name" => "list",
            "data" => Array(
                "cards" => Card::where('id', $REQUEST->id)->get()
            )
        );
        load_template($template_data);
    }

    public function create() {
        global $BREADCRUMB;

        $BREADCRUMB->addCrumb('Dashboard', '/')->addCrumb('Cartas', '/cards')->addCrumb('Novo Registro', '/cards');
        $template_data = array(
            "page_title" => "Nova Carta",
            "subsystem" => "baralho",
            "module" => "card",
            "name" => "form",
            "data" => Array(
                'card' => new Card()
            )
        );

        load_template($template_data);
    }

    public function store() {
        global $RESPONSE;

        $card = new Card();
        
        $card->name = $_POST['nome'];
        $card->suit = $_POST['naipe'];

        $card->save();

        $RESPONSE->redirect('/cards');
    }

    public function destroy() {
        global $REQUEST, $RESPONSE;

        Card::where('id', $REQUEST->id)->delete();
        $RESPONSE->redirect('/cards');
    }

}
