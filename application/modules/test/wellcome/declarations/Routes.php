<?php

$ROUTES->respond("GET", "/wellcome", function($request) {
    include_once __DIR__.'/../Controller.php';
    $controller = new Wellcome();
    $controller->index();
});

$ROUTES->respond("GET", "/teste_menu", function(){
    
    $menu = new Menu();
    $menu->add('/', "Dashboard");
    $menu->add('/cards', "Cartas");
    
    $menu_cards = new Menu();
    $menu_cards->add("/cards", "Listagem de cartas");
    $menu_cards->add("/cards/create", "Listagem de cartas");
    
    $menu->nestDirect($menu_cards);
    
    echo $menu->render();
    
    //$menu = $Menu();
    
});