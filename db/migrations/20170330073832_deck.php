<?php

use Phinx\Migration\AbstractMigration;

class Deck extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
public function up() {
       
        $suit = $this->table('suits');
        $suit->addColumn("name", "string", Array("limit" => 200));
        $suit->addTimestamps();
        $suit->save();
        
        $card = $this->table('cards');
        $card->addColumn("name", "string", Array("limit" => 200));
        $card->addColumn("suit", "integer", Array('null' => true) );
        $card->addTimestamps();
        $card->save();        
        
        $permissoes = $this->table('permissions');
        $permissoes->insert([
            ['id' => 12, 'name' => 'cards_visualizar'],
            ['id' => 13, 'name' => 'cards_inserir'],
            ['id' => 14, 'name' => 'cards_editar'],
            ['id' => 15, 'name' => 'cards_apagar'],
            
            ['id' => 16, 'name' => 'suits_visualizar'],
            ['id' => 17, 'name' => 'suits_inserir'],
            ['id' => 18, 'name' => 'suits_editar'],
            ['id' => 19, 'name' => 'suits_apagar']
        ]);
        $permissoes->saveData();
        
        $papel = $this->table('roles');

        $papel->insert([
            ['id' => 4, 'name' => 'Cards'],
            ['id' => 5, 'name' => 'Suits']
        ]);
        
        $papel->saveData();
        
        $papel_permissao = $this->table('permission_role');
        $papel_permissao->insert([
            ['role_id' => 4, 'permission_id' => 12],
            ['role_id' => 4, 'permission_id' => 13],
            ['role_id' => 4, 'permission_id' => 14],
            ['role_id' => 4, 'permission_id' => 15],
            ['role_id' => 5, 'permission_id' => 16],
            ['role_id' => 5, 'permission_id' => 17],
            ['role_id' => 5, 'permission_id' => 18],
            ['role_id' => 5, 'permission_id' => 19],
        ]);
        $papel_permissao->saveData();
        
        $papel_usuario = $this->table('role_user');
        $papel_usuario->insert([
            ['role_id' => 4, 'user_id' => 3]
        ]);
        $papel_usuario->saveData();
    }
    
    public function down(){
        $card = $this->table('cards');
        $card->drop();
        
        $suits = $this->table('suits');
        $suits->drop();
    }
}
