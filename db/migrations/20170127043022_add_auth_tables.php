<?php

use Phinx\Migration\AbstractMigration;

class AddAuthTables extends AbstractMigration {

    public function up() {
        $permissoes = $this->table('permissions');
        $permissoes->addColumn('name', "string", Array("limit" => 100, 'null' => false));
        $permissoes->addColumn('descricao', "text", Array('null' => true));
        $permissoes->create();


        $permissoes->insert([
            ['id' => 1, 'name' => 'sistema_acessar'],
            ['id' => 2, 'name' => 'administrador'],
            ['id' => 3, 'name' => 'dashboard_visualizar'],
            ['id' => 4, 'name' => 'dashboard_editar'],
            ['id' => 5, 'name' => 'dashboard_excluir'],
            ['id' => 6, 'name' => 'contas_areceber_criar'],
            ['id' => 7, 'name' => 'contas_areceber_editar'],
            ['id' => 8, 'name' => 'contas_areceber_excluir'],
            ['id' => 9, 'name' => 'rh_ponto_criar'],
            ['id' => 10, 'name' => 'rh_ponto_editar'],
            ['id' => 11, 'name' => 'rh_ponto_excluit']
        ]);
        $permissoes->saveData();


        $papel = $this->table('roles');
        $papel->addColumn("name", 'string', Array('limit' => 45));
        $papel->addColumn("descricao", 'text', Array('null' => true));
        $papel->create();

        $papel->insert([
            ['id' => 1, 'name' => 'Usuario do Sistema'],
            ['id' => 2, 'name' => 'Financeiro'],
            ['id' => 3, 'name' => 'Recursos Humanos']
        ]);
        $papel->saveData();

        $papel_permissao = $this->table('permission_role');
        $papel_permissao->addColumn("role_id", 'integer');
        $papel_permissao->addColumn("permission_id", 'integer');
        $papel_permissao->create();

        $papel_permissao->insert([
            ['role_id' => 1, 'permission_id' => 1],
            ['role_id' => 1, 'permission_id' => 3],
            ['role_id' => 2, 'permission_id' => 1],
            ['role_id' => 2, 'permission_id' => 3],
            ['role_id' => 2, 'permission_id' => 6],
            ['role_id' => 2, 'permission_id' => 7],
            ['role_id' => 2, 'permission_id' => 8],
            ['role_id' => 3, 'permission_id' => 1],
            ['role_id' => 3, 'permission_id' => 3],
            ['role_id' => 3, 'permission_id' => 9],
            ['role_id' => 3, 'permission_id' => 10],
            ['role_id' => 3, 'permission_id' => 11]
        ]);
        $papel_permissao->saveData();

        $usuarios = $this->table('users');
        $usuarios->addColumn('name', 'string', Array('limit' => 100));
        $usuarios->addColumn('username', 'string', Array('limit' => 100));
        $usuarios->addColumn('password', 'string', Array('limit' => 32));
        $usuarios->addColumn('email', 'string', Array('limit' => 200));
        $usuarios->addColumn('method', 'string', Array('limit' => 200, 'default'=>'password'));
        $usuarios->addIndex('email', Array('unique'=>true));
        $usuarios->addTimestamps();
        $usuarios->create();

        $usuarios->insert([
            ['id' => 1, 'name' => 'administrador', 'username'=>'admin', 'password' => md5('administrador'), 'email' => 'admin@sistema.com'],
            ['id' => 2, 'name' => 'João da Silva', 'username'=>'joao', 'password' => md5('joao'), 'email' => 'joao@empresa.com'],
            ['id' => 3, 'name' => 'Maria da Silva', 'username'=>'maria', 'password' => md5('maria'), 'email' => 'maria@empresa.com']
        ]);
        $usuarios->saveData();

        $papel_usuario = $this->table('role_user');
        $papel_usuario->addColumn("role_id", 'integer');
        $papel_usuario->addColumn("user_id", 'integer');        
        $papel_usuario->create();

        $papel_usuario->insert([
            ['role_id' => 1, 'user_id' => 2],
            ['role_id' => 2, 'user_id' => 2],
            ['role_id' => 1, 'user_id' => 3],
            ['role_id' => 3, 'user_id' => 3]
        ]);
        $papel_usuario->saveData();

        $permissao_usuario = $this->table('permission_user');
        $permissao_usuario->addColumn("permission_id", 'integer');
        $permissao_usuario->addColumn("user_id", 'integer');
        $permissao_usuario->addColumn("action", 'integer'); // 1-> Agregate 2->Revoke
        $permissao_usuario->create();

        $permissao_usuario->insert([
            ['permission_id' => 2, 'user_id' => 1, 'action' => 1],
            ['permission_id' => 7, 'user_id' => 3, 'action' => 1],
            ['permission_id' => 11, 'user_id' => 3, 'action' => 2]
        ]);
        $permissao_usuario->saveData();
    }
    
    public function down(){
        $this->table('permission_user')->drop();
        $this->table('role_user')->drop();
        $this->table('users')->drop();
        $this->table('permission_role')->drop();
        $this->table('roles')->drop();
        $this->table('permissions')->drop();
        
    }

}
