<?php

// DYNAMIC_ROUTING
//TRUE  - SIMPLE DISPATCH URL - LIKE CODEIGNITER
//FALSE - USE THE ROUTES
define("DYNAMIC_ROUTING", true);

// HOW THE APPLICATION WILL RESPOND
// SYSTEM - AS A SYSTEM
// API    - AS A API
define("APPLICATION_METHOD", 'SYSTEM');

//SIMPLE             - 
//MODULAR            - 
//MODULAR_SUB_SYSTEM -  
define("ROUTING_DEPTH", "MODULAR_SUB_SYSTEM");

//System will use database
//TRUE
//FALSE
define("DATABASE", TRUE);


//Auth is Required
//TRUE
//FALSE
define("AUTH_REQUIRED", TRUE);
//define("AUTH_REQUIRED", FALSE);

define("ALLOW_AJAX", TRUE);
if (ALLOW_AJAX) {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST');
}

//Defining some locations

define("ROOT_FOLDER", __DIR__);
define("LIBRARY_FOLDER", __DIR__.'/library/');
define("APPLICATION_FOLDER", __DIR__."/application/");
define("MODULES_FOLDER", __DIR__."/application/modules/");
define("TEMPLATE_FOLDER", MODULES_FOLDER.'core/');
define("GENERAL_VIEW_FOLDER", __DIR__."/application/views/");
