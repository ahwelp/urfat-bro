<?php

class Menu {

    /**
     * HTML builder instance.
     * 
     * @var \JasonLewis\Menu\HtmlBuilder
     */
    protected $html;

    /**
     * Array of attribtues.
     * 
     * @var array
     */
    protected $attributes = [];

    /**
     * Array of menu items.
     * 
     * @var array
     */
    protected $items = [];

    /**
     * Create a new Menu instance.
     * 
     * @param  array  $attributes
     * @return void
     */
    public function __construct($attributes = []) {
        $this->attributes = $attributes;
        $this->html = new HtmlBuilder;
    }

    /**
     * Add a menu item.
     * 
     * @param  string  $link
     * @param  string  $title
     * @param  array   $attributes
     * @param  int     $weight
     * @return \JasonLewis\Menu\Menu
     */
    public function add($link, $title, $icon = '', $attributes = [], $weight = 0) {
        $this->items[] = (new MenuItem)->link($link)->title($title)->icon($icon)->attributes($attributes)->weight($weight);
        return $this;
    }

    public function addDirect($menu_item) {
        $this->items[] = $menu_item;
    }

    /**
     * Nest a menu on the previous item.
     * 
     * @param  array  $attributes
     * @return \JasonLewis\Menu\Menu
     */
    public function nestMenu($attributes = []) {
        $key = count($this->items) - 1;
        $this->items[$key]->nest($menu = new Menu($attributes));
        return $menu;
    }

    /**
     * Nest a menu on the previous item.
     * 
     * @param  \JasonLewis\Menu\Menu  $menu
     * @return \JasonLewis\Menu\Menu
     */
    public function nestDirect($menu = null) {
        $key = count($this->items) - 1;
        $this->items[$key]->nest($menu);
        return $menu;
    }

    /**
     * Render the menu.
     * 
     * @return string
     */
    public function render() {
        $this->sort();
        $attributes = $this->html->attributes($this->attributes);
        $menu[] = "<ul{$attributes}>";
        foreach ($this->items as $item) {
            $menu[] = $item->render();
        }
        $menu[] = '</ul>';
        return implode(PHP_EOL, $menu);
    }

    private function sort() {
        usort($this->items, function($a, $b) { // anonymous function
            return $a->getWeight() - $b->getWeight();
        });
    }

    /**
     * Render the menu.
     * 
     * @return string
     */
    public function __toString() {
        return $this->render();
    }

}
