<?php

class Xmldb_handler {

    public static function handle() {
        $CFG = new stdClass();
        
        $CFG->usecache = false;

        $CFG->dbtype = 'mysqli';      // 'pgsql', 'mariadb', 'mysqli', 'mssql', 'sqlsrv' or 'oci'
        $CFG->dblibrary = 'native';      // 'native' only at the moment
        $CFG->dbhost = 'localhost';  // eg 'localhost' or 'db.isp.com' or IP
        $CFG->dbname = 'moodle';     // database name, eg moodle
        $CFG->dbuser = 'root';    // your database username
        $CFG->dbpass = 'root';    // your database password
        $CFG->prefix = '';        // prefix to use for all table names
        $CFG->dboptions = array(
            'dbpersist' => false,
            'dbsocket' => false,
            'dbport' => '',
        );

        require_once('setuplib.php');
        require_once('dmllib.php');

        setup_DB();

        require_once('ddllib.php');

        //$base_path = APPPATH . 'modules/';
        //$file_name_string = '/^.+\upgrade.php$/i';

        //echo "<pre>";
        //var_dump(glob($base_path . "*.{.php}", GLOB_BRACE));

        $DB->get_manager()->manage_changes('install.xml');
        //$DB->get_manager()->install_from_xmldb_file($CFG->libdir.'/install.xml');
        //$file = $DB->get_manager()->load_xmldb_file($CFG->libdir.'/install.xml');
        //echo "<pre>";
        //var_dump($DB->get_manager()->check_database_schema($file->getStructure()));
        //$arr = new xmldb_structure($CFG->libdir.'/install.xml');
        //$arr->arr2xmldb_structure($CFG->libdir.'/install.xml');
        //$DB->get_manager()->check_database_schema($arr);
    }
}
