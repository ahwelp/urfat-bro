<?php

/*
 * ci_events
 *
 * This library is part of ci_fruit_salad https://github.com/ahwelp/ci_fruit_salad
 * This library was "designed" to be an easy event handler
 *
 * Requires Alternative PHP Cache (APC) http://php.net/manual/pt_BR/book.apc.php
 * You can install it with "apt-get install php-apc"
 *
 * The code is based on Moodle's Events 2
 *
 * You can do what ever you want with this if, you think usable.
 * Just remember me.
 *
 * arturh@viavale.com.br
 */

class Events {

    function __construct() {
        $this->load_listeners();
    }

    /**
     * Function to trigger an event
     *
     * In the code you can execute it with Events::trigger_event('event_name', 'data_to_function')
     *
     * @param  string  $event_name
     * @param  array  $event_data
     *
     */
    public static function trigger_event($event_name = null, $event_data = null) {
        $listeners = apc_fetch('events');
        $base_path = APPPATH;
        //$base_path = /var/www/html/ci_fruit_salad;
        //Look if sameone want the event, if not return
        if (!isset($listeners[$event_name])) {
            return;
        }

        $listeners = $listeners[$event_name];

        /*
         * 	Todo, Order the listeners by priority
         */

        foreach ($listeners as $listener) {

            //load the defined file
            require_once $base_path . $listener['file_load'];

            //if the function is not set on the file, won't execute it
            if ($listener['method'] != '') {
                call_user_func($listener['method'], $event_data);
            }
        }
    }

    /**
     * Function to clear the cache
     *
     * This function will clear the cache and force
     * the library to reload the listeners
     *
     * I recomend to reload the page after this
     * be executed
     */
    public static function reload_listeners() {
        apc_store('events_update', true);
    }

    /**
     * Function to load the listeners
     *
     * This function will recursively locate in a defined
     * folder for files to load process and cache
     *
     * This funtion is executed in part on all requests.
     * One it is executed, it will cache the result and
     * wont load files until the cache is clear or flagged
     *
     * @param  string  $event_name
     * @param  array  $event_data
     *
     */
    private function load_listeners() {
        if (apc_fetch('events_update') === true || apc_fetch('events') === false) {

            echo "Loading";

            //Define where and what to look for
            $base_path = APPPATH . 'modules';
            $file_name_string = 'listener.json';

            //Look inside every folder for the defined file
            //Nice informations: http://stackoverflow.com/questions/15054997/find-all-php-files-in-folder-recursively
            $di = new RecursiveDirectoryIterator($base_path, RecursiveDirectoryIterator::SKIP_DOTS);
            $it = new RecursiveIteratorIterator($di);

            $events_block = array();
            $event_list = array();

            //Read and covert to array every file that match
            foreach ($it as $file) {
                if (pathinfo($file, PATHINFO_BASENAME) == "listener.json") {
                    $file_content = file_get_contents($file);
                    $events_block[] = json_decode($file_content, true);
                }
            }

            /*
             * Filter the genereted array in a new clear array
             *
             * The event_list keys will be the events name, inside this array
             * will exist every listener to it
             */
            foreach ($events_block as $events) {
                foreach ($events as $event) {
                    foreach ($event as $single_event) {
                        $event_list[$single_event['listener']][] = $single_event;
                    }
                }
            }

            // Store in cache the event list and a flag that will prevent
            // from this be executed allways.
            // If you want to force the system to reload the events
            // set events_update to true with the function reload_listeners()
            apc_store("events", $event_list);
            apc_store("events_update", false);
        }
    }
}

//Magic happening. ????Rigth way????
$events = new Events();
